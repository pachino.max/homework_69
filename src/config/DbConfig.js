import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyAvn5r_BcdMCGyDAgB5Vo_LAIQl1q0i7xc",
    authDomain: "myblog-bb75e.firebaseapp.com",
    databaseURL: "https://myblog-bb75e.firebaseio.com",
    projectId: "myblog-bb75e",
    storageBucket: "myblog-bb75e.appspot.com",
    messagingSenderId: "731261633386"
  };
  
  firebase.initializeApp(firebaseConfig);
  firebase.firestore().settings({timestampsInSnapshots: true});

  export default firebase;