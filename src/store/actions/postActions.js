export const createPost = (post) => {
    return (dispatch, getState, { getFirebase,  getFirestore }) => {
        const firestore = getFirestore();
        const profile = getState().firebase.profile
        const authorId = getState().firebase.auth.uid
        firestore.collection('posts').add({
            ...post,
            authorFirstName: profile.firstName,
            authorLastName: profile.lastName,
            authorId: authorId,
            createdAt: new Date()

        }).then(() => {
            dispatch({ type: 'CREATE_POST', post })
        }
        ).catch((err) => {
            dispatch({ type: 'CREATE_POST_ERROR', err })
        })

    }
}

export const removePost = (post) => {
    return (dispatch, getState, { getFirebase,getFirestore }) => {
        const firestore = getFirestore();
        const profile = getState().firebase.profile
        const authorId = getState().firebase.auth.uid
        firestore.collection('posts').delete({
            ...post,
            authorFirstName: profile.firstName,
            authorLastName: profile.lastName,
            authorId: authorId,
            createdAt: new Date()

        }).then(() => {
            dispatch({ type: 'REMOVE_POST', post })
        }
        ).catch((err) => {
            dispatch({ type: 'REMOVE_POST_ERROR', err })
        })
    }
}