const initState = {
    posts: [
        { id: 1, title: 'Something incredible happened just now', description: 'La La La' },
        { id: 2, title: 'Another blog title', description: 'La La La' },
        { id: 3, title: 'Yet another blog title', description: 'La La La' }
    ]
}

const postReducer = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_POST':
            console.log('created post', action.post)
            return state;
        case 'CREATE_POST_ERROR':
            console.log('create post error', action.err)
            return state;
        case 'REMOVE_POST':
            console.log('Removed post')
            return state
        case 'REMOVE_POST_ERROR':
            console.log('Removed post Error', action.err)
            return state
        default:
            return state;
    }

}

export default postReducer