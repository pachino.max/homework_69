const initState = {
    authError: null
}

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case 'LOGIN_ERROR':
            console.log('login error')
            return {
                ...state,
                authError: 'Login failed'
            }
        case 'LOGIN_SUCCESS':
            console.log('login success')
            return {
                ...state,
                authError: null
            }
        case 'SIGNOUT_SUCCESS':
            console.log('sign out success')
            return state;
        case 'REGISTER_SUCCESS':
            console.log('register success')
            return{
                ...state,
                authError:null
            }
        case 'REGITER_ERROR':
            console.log('register error')
            return{
                ...state,
                authError: action.err.message
            }
        default:
            return state;
    }
    
}

export default authReducer