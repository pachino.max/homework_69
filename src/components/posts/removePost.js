import React, { Component } from "react";
import { connect } from "react-redux";
import { removePost } from "../../store/actions/postActions";

const RemovePost = ({ post }) => {

  const handleDelete = (e) => {
    e.preventDefault();
    this.props.removePost(post);
    this.props.history.push("/")

  return (
    <div>
      <button onClick={this.handleDelete} className="btn green z-depth-0">
        Delete
      </button>
    </div>
  );
};
}


const mapDispatchToProps = dispatch => {
  return {
    removePost: post => dispatch(removePost(post))
  };
};

export default connect(null, mapDispatchToProps)(RemovePost)