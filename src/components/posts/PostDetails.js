import React, {Component}from 'react'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect} from 'react-router-dom'
import moment from 'moment'
import { removePost } from '../../store/actions/postActions'

class PostDetails extends Component  {
    
    handleChange = () => {
        console.log(this.state)
        this.props.removePost(this.state)
        this.props.history.push('/')

    }
    render(){
    const { post, auth} = this.props;
    
    if(!auth.uid) return <Redirect to='/login' />
    if (post) {
        return(
        <div className='container section post-details'>
            <div className='card z-depth-0'>
                <div className='card-content'>
                    <span className='card-title'>{post.title}</span>
                    <p> {post.description}</p>
                </div>
                <div className='card-action grey lighten-4 grey-text'>
                    <div>Posted by: {post.authorFirstName} {post.authorLastName}</div>
                    <div>{moment(post.createdAt.toDate()).calendar()}</div>
                    <div className='input-field'>
                        <button onChange={this.handleChange} className='btn red lighten-1 z-depth-0'>Delete Post</button>
                    </div>
                    <div className='input-field'>
                        <button onChange={this.handleChange} className='btn yellow lighten-1 z-depth-0'>Edit Post</button>
                    </div>
                    
                </div>
            </div>

        </div>)
    }
    else{

    
    return (
        <div className='container center'>
            <p>Loading post.....</p>

        </div>
    )
    
    }}}


const mapStateToProps = (state, ownProps) => {
    console.log(state)
    const id = ownProps.match.params.id;
    const posts = state.firestore.data.posts
    const post = posts ? posts[id] : null
    return {
        post: post,
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removePost: (post) => dispatch(removePost(post))
    }
}
export default compose(
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([{ collection: "posts" }])
)(PostDetails)