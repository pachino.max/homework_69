import React from 'react'

const About =() =>{
    return(
        <div className='container'>
            <h1 className='grey lighten-2 text center'>Welcome to MY BLOG</h1>
            <p className='grey lighten-2'>At MyBlog.com, we believe everyone deserves to have a website or online store.
                 Innovation and simplicity makes us happy: our goal is to remove any technical 
                 or financial barriers that can prevent business owners from making their own 
                 website. We're excited to help you on your journey!</p>
        </div>
    )
}
export default About