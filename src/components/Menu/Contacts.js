import React from 'react'

const Contacts = () => {
    return (
        <div className='container'>
            <h1 className='grey lighten-2 text center'>Contact Us</h1>
            <ul className='grey lighten-2'>BY PHONE
            <li>(Monday to Friday, 9am to 6pm PST)  North America Toll-Free:
                1-877-930-7483                 </li>
                <li>International:1-604-637-0780</li>
            </ul>

        </div>
    )
}
export default Contacts