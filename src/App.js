import React from 'react';
import {BrowserRouter,Switch,Route} from 'react-router-dom'
import Navbar from './components/navbar/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import PostDetails from './components/posts/PostDetails';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import CreatePost from './components/posts/CreatePost';
import Home from './components/Menu/Home';
import About from './components/Menu/About';
import Contacts from './components/Menu/Contacts';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path='/' component={Dashboard}></Route>
        <Route path='/post/:id' component={PostDetails}></Route>
        <Route path='/login' component={Login}></Route>
        <Route path='/register' component={Register}></Route>
        <Route path='/posts/add' component={CreatePost}></Route>
        <Route path='/home' component={Home} />
        <Route path='/about' component={About} />
        <Route path='/contacts' component={Contacts} />
      </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
